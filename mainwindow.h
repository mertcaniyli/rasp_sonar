#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <wiringPi.h>
#include <QPainter>
#include <QString>
#include "libSonar.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent);
    ~MainWindow();
    void move();
    int getPointX() const;
    void setPointX(int pointX);
    void keyPressEvent(QKeyEvent *event);
    virtual void paintEvent(QPaintEvent *event);

private:
    Ui::MainWindow *ui;
    int trigger = 1;
    int echo = 0;
    Sonar sonar;
    int dist=51;



};

#endif // MAINWINDOW_H

